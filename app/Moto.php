<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Moto extends Model
{
    protected $fillable =[
        'title',
        'description',
        'video',
        'image',
    ];
}
