<?php

namespace App\Http\Controllers\Back;
use App\Testimonial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $testimonials = Testimonial::orderBy('updated_at', 'desc')->get();
        return view('back.testimonial.index', compact('testimonials'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $testimonial = Testimonial::create($this->validateRequest());
        $this->storeImage($testimonial);
        return redirect()->back()->with('success', 'Data for new Testimonial stored Successfully!!');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Testimonial $testimonial)
    {
       return view('back.testimonial.edit', compact('testimonial'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Testimonial $testimonial, Request $request )
    {
        $testimonial->update($this->validateRequest());
        $testimonial->storeImage($testimonial);
        return redirect()->route('testimonial.create')->with('success', 'Data for Testimonial Updated Successfully!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Testimonial $testimonial)
    {
        Storage::delete($testimonial->image);
        $testimonial->delete();
        return redirect()->back()->with('success', 'Testimonial Deleted Successfully!!!');
    }

    private function validateRequest()
    {
        return request()->validate([
            'name'        => 'required|min:3',
            'post'        => 'required|min:3',
            'image'       => 'required|sometimes|mimes:jpeg,png,jpg|max:3000',
            'description' => 'required|min:10',
        ]);
    } 

    private function storeImage($testimonial)
    {
        if(request()->has('image')) {
            $testimonial->update([
                'image' => request()->image->store('testimonial', 'public'),
            ]);
        }
    }
}
