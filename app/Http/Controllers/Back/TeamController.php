<?php

namespace App\Http\Controllers\Back;
use App\Team;
use Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teams = Team::orderBy('updated_at', 'desc')->get();
        return view('back.team.index',compact('teams'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $team = Team::create($this->validateRequest());
        $this->storeImage($team);
        return redirect()->back()->with('success', 'Team Stored Successfully!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Team $team)
    {
        return view('back.team.edit', compact('team'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Team $team)
    {
        $team->update($this->validateRequest());

        $this->storeImage($team);

        return redirect()->route('team.create')->with('success', 'Team Updated Successfully!!!'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Team $team)
    {
        Storage::delete($team->image);
        $team->delete();
        return redirect()->back()->with('success', 'Team Deleted Successfully!!!');
    }

    private function validateRequest()
    {
        return request()->validate([
            'post_with_company' => 'required|sometimes|min:5|max:50',
            'image'             => 'required|sometimes|mimes:jpeg,png,jpg|max:3000',
            'description'       => 'required|sometimes|min:10',
            'name'              => 'required|sometimes|min:3|max:20',
        ]);
    }

    private function storeImage($team)
    {
        if(request()->has('image')) {
            $team->update([
                'image' => request()->image->store('team', 'public'),
            ]);
        }
    }
}
