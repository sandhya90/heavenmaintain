<?php

namespace App\Http\Controllers\Back;

use App\Joinus;
//use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class JoinusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [];

        $Validator = Validator::make(
            $request->all(),
            [
                'full_name' => 'required|max:50',
                'email' => 'required|unique:joinuses|email',
                'contact' => 'required|min:10|max:10',
                'address' => 'required|max:20',
                'description' => 'required|max:100',
            ],
            $messages
        );

        if($Validator->fails()){
            $Response = $Validator->messages();
        } else {
            $joinus = new Joinus;
            $joinus->full_name = $request->full_name;
            $joinus->email = $request->email;
            $joinus->contact = $request->contact;
            $joinus->address = $request->address;
            $joinus->description = $request->description;
            $joinus->save();
            $Response = ['success' => 'Your Action has been Successfully Done.'];
        }
//        return redirect()->back()->with('success', 'Your Details Sent Successfully');
        return response()->json($Response, 200);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $joinuses = Joinus::orderBy('updated_at', 'desc')->get();
        return view('back.joinus.joinus', compact('joinuses'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Joinus $joinus)
    {
        $joinus->delete();
        return redirect()->back()->with('success', 'Data Deleted Successfully!!!');
    }
}
