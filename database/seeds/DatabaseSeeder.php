<?php
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call('UsersTableSeeder');
        $this->command->info('table seeded!');
    }
}
class UsersTableSeeder extends Seeder {
    public function run()
    {
        DB::table('users')->delete();
        User::create([
            'name' => 'Sandhya Oli',
            'email' => 'olisandhya707@gmail.com',
            'address' => 'Kapan',
            'phone' => '9860979838',
            'password' => Hash::make('password')]);
    }
}
