$('.index-fourth-sec .owl-carousel').owlCarousel({
    items: 1,
    margin: 0,
    loop: true,
    nav: false,
    dots: false,
    dotsEach: false,
    autoplay: true,
    autoplaySpeed: 1500,
    navSpeed: 1500,
    autoplayTimeout: 4000,
    autoplayHoverPause: true
});

$('.index-third-sec .owl-carousel').owlCarousel({
    items: 3,
    margin: 30,
    loop: true,
    nav: true,
    autoplay: true,
    autoplaySpeed: 1500,
    navSpeed: 1500,
    autoplayTimeout: 4000,
    autoplayHoverPause: true,
    responsive: {
        0: {
            items: 1,
        },
        750: {
            items: 1,
        },
        1000: {
            items: 3,
        }
    }
});


// Validation and success message for JoinUs Modal Form 
$(function(){
    $('#join-us-form').submit(function (e) {
        var route = $('#join-us-form').data('route');
        var form_data = $(this);
        $('.alert').remove();
        $.ajax({
            type: 'POST',
            url: route,
            data: form_data.serialize(),
            success: function(Response) {
                // console.log(Response);
                if(Response.full_name){
                    $('#full_name').append('<small class="alert text-danger">' +Response.full_name+ '</small>');
                }
                if(Response.email){
                    $('#email').append('<small class="alert text-danger">' +Response.email+ '</small>');
                }
                if(Response.contact){
                    $('#contact').append('<small class="alert text-danger">' +Response.contact+ '</small>');
                }
                if(Response.address){
                    $('#address').append('<small class="alert text-danger">' +Response.address+ '</small>');
                }
                if(Response.description){
                    $('#description').append('<small class="alert text-danger">' +Response.description+ '</small>');
                }
                if(Response.success){
                    $('#success').append('<small class="alert text-success">' +Response.success+ '</small>')
                }
            }
        });

        e.preventDefault();
    });
});


// Validation and Success Message for Newsletter Subsciption
$(function(){
    $('#newsletter-form').submit(function (e) {
        var route = $('#newsletter-form').data('route');
        var form_data = $(this);
        $('.alert').remove();
        $.ajax({
            type: 'POST',
            url: route,
            data: form_data.serialize(),
            success: function(Response){
                // console.log(Response);
                if(Response.email){
                    $('#newsletter-form').append('<small class="alert text-danger">' +Response.email+ '</small>');
                }
                if(Response.success){
                    $('.newsletter-title-p').css({'color': '#79d70f', 'font-weight': 'bold'}).html('Thank You for Your Subsciption.');
                }
            }
        });
        e.preventDefault();
    });
});