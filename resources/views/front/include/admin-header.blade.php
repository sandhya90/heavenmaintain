<!DOCTYPE html>
<html lang="en">

 
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <title>Heaven Maker</title>
	  
    <link href="{{asset('front/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css" >
    <link href="{{asset('front/css/responsive.dataTables.min.css')}}" rel="stylesheet" type="text/css" >
    <link href="{{asset('front/css/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('front/css/summernote.css')}}" rel="stylesheet" /> 
    <link href="{{asset('front/css/bootstrap-timepicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/daterangepicker.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/steps.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/select2.min.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/user.css')}}" rel="stylesheet"> 
    <link href="{{asset('front/css/admin.css')}}" rel="stylesheet">    
    <link href="{{asset('front/css/colors/blue.css')}}" id="theme" rel="stylesheet">
 
</head>

<body class="fix-header card-no-border logo-center"> 
    <div id="main-wrapper"> 
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light"> 
                <div class="navbar-header header-logo-div">
                    <a class="navbar-brand header-logo-a" href="index.php"> 
                        <img src="../images/logo-01.png" alt="homepage" class="img-fluid" /> 
                    </a> 
                </div> 
                <div class="navbar-collapse"> 
                    <ul class="navbar-nav mr-auto mt-md-0">  
                        <li class="nav-item"> 
                            <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)">
                                <i class="mdi mdi-menu"></i>
                            </a> 
                        </li>
                        <li class="nav-item hidden-sm-down search-box">
                            <a class="nav-link hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)">
                                <i class="fa fa-search"></i>
                            </a>
                            <form class="app-search">
                                <input type="text" class="form-control" placeholder="Search & enter"> 
                                <a class="srh-btn"><i class="fa fa-times"></i></a> 
                            </form>
                        </li>  
                    </ul> 
                    <ul class="navbar-nav my-lg-0"> 
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted text-muted waves-effect waves-dark" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                                <i class="mdi mdi-bell"></i>
                                <div class="notify"> 
                                    <span class="heartbit"></span> 
                                    <span class="point"></span> 
                                </div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right mailbox scale-up">
                                <ul>
                                    <li>
                                        <div class="drop-title">Notifications</div>
                                    </li>
                                    <li>
                                        <div class="message-center"> 
                                            <a href="#">
                                                <div class="btn btn-danger btn-circle">
                                                    <i class="fa fa-link"></i>
                                                </div>
                                                <div class="mail-contnet">
                                                    <h5>Luanch Admin</h5> 
                                                    <span class="mail-desc">
                                                        Just see the my new admin!
                                                    </span> 
                                                    <span class="time">9:30 AM</span> 
                                                </div>
                                            </a> 
                                            <a href="#">
                                                <div class="btn btn-success btn-circle">
                                                    <i class="ti-calendar"></i>
                                                </div>
                                                <div class="mail-contnet">
                                                    <h5>Event today</h5> 
                                                    <span class="mail-desc">
                                                        Just a reminder that you have event
                                                    </span> 
                                                    <span class="time">9:10 AM</span> 
                                                </div>
                                            </a> 
                                            <a href="#">
                                                <div class="btn btn-info btn-circle">
                                                    <i class="ti-settings"></i>
                                                </div>
                                                <div class="mail-contnet">
                                                    <h5>Settings</h5> 
                                                    <span class="mail-desc">
                                                        You can customize this template as you want
                                                    </span> 
                                                    <span class="time">9:08 AM</span> 
                                                </div>
                                            </a> 
                                            <a href="#">
                                                <div class="btn btn-primary btn-circle">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <div class="mail-contnet">
                                                    <h5>Pavan kumar</h5> 
                                                    <span class="mail-desc">
                                                        Just see the my admin!
                                                    </span> 
                                                    <span class="time">9:02 AM</span> 
                                                </div>
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <a class="nav-link text-center" href="notification.php"> 
                                            <strong>Check all notifications</strong> 
                                            <i class="fa fa-angle-right"></i> 
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="../images/users/1.jpg" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box text-center"> 
                                            <div class="u-text">
                                                <h4>Gopal Basnet</h4>
                                                <p class="text-muted">info@heavenmaker.com</p> 
                                            </div>
                                        </div>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="profile.php"><i class="ti-user"></i> My Profile</a></li>  
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li> 
                    </ul>
                </div>
            </nav>
        </header>