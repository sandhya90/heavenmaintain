@include('front.include.header')

	<div id="main-wrapper">
		<section class="section top-header-section">
			<div class="overlay-header"></div>
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="logo-head">
							<a href="index.php">
								<img src="front/images/frontend/logo-hm-white-03.png" class="img-fluid text-center">
							</a>
						</div>
		            </div>
		            <div class="col-lg-12">
			            <div class="select-service-wrapper">
			            	<h4 class="select-service-title">Select Service</h4>
			            	<div class="select-service-items-wrapper">
			            		<div class="select-service-items">
			            			<div class="service-img">
			            				<a href="https://www.alpasal.com/">
			            					<img src="front/images/frontend/service-logo-03.png" class="img-fluid">
			            				</a>
			            			</div>
			            			<div class="service-name">
			            				<a href="https://www.alpasal.com/">Alpasal</a>
			            			</div>
			            		</div>
			            		<div class="select-service-items">
			            			<div class="service-img">
			            				<a href="https://www.alkrishi.com/">
			            					<img src="front/images/frontend/service-logo-01.png" class="img-fluid">
			            				</a>
			            			</div>
			            			<div class="service-name">
			            				<a href="https://www.alkrishi.com/">Alkrishi</a>
			            			</div>
			            		</div>
			            		<div class="select-service-items">
			            			<div class="service-img">
			            				<a href="http://omlotexpress.com/">
				            				<img src="front/images/frontend/service-logo-02.png" class="img-fluid" >
				            			</a>
			            			</div>
			            			<div class="service-name">
			            				<a href="http://omlotexpress.com/">Omlot Express</a>
			            			</div>
			            		</div>
			            		<div class="select-service-items">
			            			<div class="service-img">
			            				<a href="http://omlotstate.com/">
				            				<img src="front/images/frontend/service-logo-04.png" class="img-fluid">
				            			</a>
			            			</div>
			            			<div class="service-name">
			            				<a href="http://omlotstate.com/">Omlot State</a>
			            			</div>
			            		</div>
			            	</div>
			            </div>
			        </div>
			        <div class="col-lg-12 d-flex justify-content-center">
			        	<div class="header-navi">
			        		<ul class="header-ul">
			        			<li>
			        				<a href="{{ route('contact') }}" class="active">
			        					<i class="fas fa-mobile-alt"></i>
			        					Contact Us
			        				</a>
			        			</li>
			        			<li>
			        				<a href="join-us.php">
			        					<i class="fas fa-handshake"></i>
			        					Join Us
			        				</a>
			        			</li>
			        		</ul>
			        	</div>
			        </div>
				</div>
			</div>
		</section>

		<section class="section contact-sec">
			<div class="container">
				<div class="row">
					<div class="col-lg-8">
						<div class="get-touch-wrapper">
							<h3>Get In Touch</h3>
							<div class="get-touch-main">
								<form action="" class="row">
									<div class="form-group col-lg-6">
										<label for="">Full Name</label>
										<input type="text" class="form-control">
									</div>
									<div class="form-group col-lg-6">
										<label for="">Email</label>
										<input type="text" class="form-control">
									</div>
									<div class="form-group col-lg-12">
										<label for="">Description</label>
										<textarea name="" class="form-control" id="" cols="10" rows="5"></textarea>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="ceo-info-wrapper">
							<h3>Contact With CEO's</h3>
							<div class="ceo-info-div">
								<div class="ceo-items">
									<h5>Gopal Basnet</h5>
									<ul>
										<li>
											<i class="fas fa-mobile-alt"></i> :
											<span>9844606728</span>
										</li>
										<li>
											<i class="far fa-envelope"></i> :
											<span>basnet.gopal12@gmail.com</span>
										</li>
									</ul>
								</div>
								<div class="ceo-items">
									<h5>Gopal Basnet</h5>
									<ul>
										<li>
											<i class="fas fa-mobile-alt"></i> :
											<span>9844606728</span>
										</li>
										<li>
											<i class="far fa-envelope"></i> :
											<span>basnet.gopal12@gmail.com</span>
										</li>
									</ul>
								</div>
								<div class="ceo-items">
									<h5>Gopal Basnet</h5>
									<ul>
										<li>
											<i class="fas fa-mobile-alt"></i> :
											<span>9844606728</span>
										</li>
										<li>
											<i class="far fa-envelope"></i> :
											<span>basnet.gopal12@gmail.com</span>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row m-t-10">
					<!-- <div class="col-lg-12">
						<h3 class="branche-h3">Our Branches</h3>
					</div> -->
					<div class="col-lg-4">
						<div class="address-wrapper">
							<div class="map-desc">
								<h3>
									Kathmandu Branch
								</h3>
								<ul>
									<li>
										<i class="fas fa-mobile-alt"></i> :
										<span>9844606728</span>
									</li>
									<li>
										<i class="far fa-envelope"></i> :
										<span>basnet.gopal12@gmail.com</span>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="address-wrapper">
							<div class="map-desc">
								<h3>
									Butwal Branch
								</h3>
								<ul>
									<li>
										<i class="fas fa-mobile-alt"></i> :
										<span>9844606728</span>
									</li>
									<li>
										<i class="far fa-envelope"></i> :
										<span>basnet.gopal12@gmail.com</span>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="address-wrapper">
							<div class="map-desc">
								<h3>
									India Branch
								</h3>
								<ul>
									<li>
										<i class="fas fa-mobile-alt"></i> :
										<span>9844606728</span>
									</li>
									<li>
										<i class="far fa-envelope"></i> :
										<span>basnet.gopal12@gmail.com</span>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="section index-fifth-sec">
			<div class="newsletter-overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="newsletter-wrapper">
							<div class="newsletter-div-main row">
								<div class="col-lg-6 col-lg-offset-3">
									<form action="" class="newletter-form-index">
										<div class="input-group">
										  	<div class="input-group-prepend">
										    	<span class="input-group-text" id="inputGroup-sizing-default">
										    		<i class="far fa-envelope"></i>
										    	</span>
										  	</div>
											<input type="email" class="form-control" placeholder="Email" aria-label="email" aria-describedby="">
										  	<div class="input-group-append">
										    	<button class="btn btn-outline-secondary" type="button">
										    		Subscribe
										    	</button>
										  	</div>
										</div>
									</form>
								</div>
								<div class="col-lg-6 col-lg-offset-3">
									<p class="newsletter-title-p">
										Enter your email for subscribe to get monthly Newsletter
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>

	@include('front.include.footer')
