@extends('back.include.layout')
@section('content')
<div class="row p-t-20">
    <div class="col-lg-4 col-xlg-3 col-md-5">
        <div class="card">
            <div class="card-body p-t-0 p-b-0">
                <center class="m-t-20">
                    <h4 class="card-title m-t-10">{{ $user->name }}</h4>
                    <h6 class="card-subtitle">Super Admin</h6>
                </center>
            </div>
            <hr>
            <div class="card-body p-t-0">
                <small class="text-muted">Email address </small>
                <h6>{{ $user->email }}</h6>
                <small class="text-muted db">Phone</small><h6>{{ $user->phone }}</h6>
                <small class="text-muted db">Address</small><h6>{{ $user->address }}</h6>
            </div>
        </div>
    </div>
    <div class="col-lg-8 col-xlg-9 col-md-7">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Edit Profile</h4>
                <hr>
                {{Form::open(['method'=>'patch', 'route'=>['profile.update', $user->id], 'enctype' => 'multipart/form-data'])}}
                <div class="form-body">
                    <div class="row p-t-20">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Full Name</label>
                                <input type="text" name="name" value="{{ old('name') ?? $user->name }}" id="fullName" class="form-control" placeholder="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Address</label>
                                <input type="text" name="address" value="{{ old('address') ?? $user->address }}" id="adminAddress" class="form-control" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Email Address</label>
                                <input type="email" name="email" value="{{ old('email') ?? $user->email }}" id="adminEmail" class="form-control" placeholder="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Contact Number </label>
                                <input type="number" name="phone" value="{{ old('phone') ?? $user->phone }}" id="adminNumber-1" class="form-control" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Change Password</label>
                                <input type="password" name="password" value="{{ old('password') ?? $user->password }}" id="password" class="form-control" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions m-t-20">
                    <button type="submit" class="btn btn-success"> Submit </button>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
</div>
@stop