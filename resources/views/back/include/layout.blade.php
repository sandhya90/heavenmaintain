@include('back.include.header')
@include('back.include.navbar')
@include('back.include.sidenavbar')


<div class="container-fluid">
        @if(session()->has('success'))
            <div class="alert alert-success">
                <p>{{ session()->get('success') }}</p>
            </div>
        @endif

        @if(session()->has('error'))
            <div class="alert alert-danger">
                <p>{{ session()->get('error') }}</p>
            </div>
        @endif

        @yield('content')
</div>
</div>

@include('back.include.footer')

