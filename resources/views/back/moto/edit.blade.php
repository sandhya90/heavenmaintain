@extends('back.include.layout')
@section('content')

<div class = "container">
    <h2>Edit Moto</h2>
    {{Form::open(['method'=>'patch', 'route'=>['moto.update', $moto->id], 'enctype' => 'multipart/form-data'])}}
        <div class = "form-group">
            <label>Title</label>
            <input type="string"  name='title' value = "{{ old('title') ?? $moto->title }}" class="form-control @error('title') is-invalid @enderror" required><br>
            @error('title')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class = "form-group">
            <label>Description</label>
            <textarea name='description' class="form-control @error('description') is-invalid @enderror" required>{{ old('description') ?? $moto->description}}</textarea>
            @error('description')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class = "form-group">
            <label>Video</label>
            <input type="file"  name='video' value = "{{ old('video') ?? $moto->video}}" class="form-control @error('video') is-invalid @enderror" >{{ $moto->video }}<br>
            @error('video')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    <div class = "form-group">
        <label>Thumbnail</label>
        <input type="file"  name='image' value = "{{ old('image') ?? $moto->image}}" class="form-control @error('image') is-invalid @enderror" >{{ $moto->image }}<br>
        @error('image')
        <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
        @enderror
    </div>
        <button class="btn btn-primary btn-sm" type="submit">Update</button>
    {{Form::close()}}
</div>

@stop