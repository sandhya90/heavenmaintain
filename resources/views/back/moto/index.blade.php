@extends('back.include.layout')
@section('content')

<div class = "container">
<div id="moto-form">
<h2 class="text-center">Create New Moto</h2>

  {{Form::open(['method'=>'post', 'route'=>'moto.store', 'enctype' => 'multipart/form-data'])}}

    <div class = "form-group {{ $errors->has('title') ? ' has-error' : '' }}">
        <label>Title</label>
        <input type="string"  name="title" value="{{ old('title') }}" class="form-control"><br>
        <small class="text-danger">{{ $errors->first('title') }}</small>
    </div>

    <div class = "form-group {{ $errors->has('description') ? ' has-error' : '' }}">
        <label>Description</label>
        <textarea name='description' class="form-control">{{ old('description') }}</textarea>
        <small class="text-danger">{{ $errors->first('description') }}</small>
    </div>
    
    <div class = "form-group {{ $errors->has('video') ? ' has-error' : '' }}">
        <label>Video</label>
        <input type="file"  name='video' value="{{ old('video') }}" class="form-control"><br>
        <small class="text-danger">{{ $errors->first('video') }}</small>
    </div>
    
    <div class = "form-group {{ $errors->has('image') ? ' has-error' : '' }}">
        <label>Thumbnail</label>
        <input type="file"  name='image' value="{{ old('image') }}" class="form-control"><br>
        <small class="text-danger">{{ $errors->first('image') }}</small>
    </div>

    <button class="btn btn-primary btn-sm" type="submit">Submit</button>

  {{Form::close()}}

</div>

<h3 class="text-center">List of Moto</h3>
<div id="moto-list">
<table class="table mt-5">

      <thead>
        <tr>
          <th scope="col">S.N.</th>
          <th scope="col">Title</th>
          <th scope="col">Description</th>
          <th scope="col">Thumbnail</th>
          <th scope="col">Video</th>
          <th scope="col">Action</th>
        </tr>
      </thead>

      <tbody>
        @php($i = 1)
        @foreach($motos as $moto)
          <tr>
            <th scope="row">{{$i++}}</th>
            <td>{{$moto->title}}</td>
            <td>{{$moto->description}}</td>
            <td>Moto Image</td>
            <td>Moto Video</td>

            <td>
              <a href = "{{route('moto.edit', $moto->id)}}" class = "btn btn-success btn-sm">Edit</a>
              <a href = "{{route('moto.destroy', $moto->id)}}" class = "btn btn-danger btn-sm">Delete</a>

            </td>
          </tr>
        @endforeach
      </tbody>
      
    </table>
</div>
  

</div>
@stop


