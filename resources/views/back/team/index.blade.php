@extends('back.include.layout')
@section('content')


<div class = "container">

<div id="team-form">
  <h2 class="text-center">Add a New Team Member</h2>
  {{Form::open(['method'=>'post', 'route'=>'team.store', 'enctype' => 'multipart/form-data'])}}
    <div class = "form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        <label>Name</label>   
        <input type="string"  name='name' value ="{{old('name')}}" class="form-control" placeholder="Name"><br>
        <small class="text-danger">{{ $errors->first('name') }}</small>
    </div>
    <div class = "form-group{{ $errors->has('post_with_company') ? ' has-error' : '' }}">
        <label>Post and Company Name</label>   
        <input type="string"  name='post_with_company' value ="{{old('post_with_company')}}" class="form-control" placeholder="Post, CompanyName"><br>
        <small class="text-danger">{{ $errors->first('post_with_company') }}</small>
    </div>
    <div class = "form-group {{ $errors->has('image') ? ' has-error' : '' }}">
        <label>Image</label>   
        <input type="file"  name='image' value ="{{old('image')}}" class="form-control"><br>
        <small class="text-danger">{{ $errors->first('image') }}</small>
    </div>
    <div class = "form-group{{ $errors->has('description') ? ' has-error' : '' }}">
        <label>Description</label>   
        <textarea name='description' class="form-control" placeholder="Description">{{old('description')}}</textarea>
        <small class="text-danger">{{ $errors->first('description') }}</small>
    </div>
    <button class="btn btn-primary btn-sm" type="submit">Submit</button>
  {{Form::close()}}
</div>





<h3 class="text-center">List of Team</h3>
<table class="table mt-5">
        <thead>
          <tr>
            <th scope="col">S.N.</th>
            <th scope="col"> Name</th>
            <th scope="col">Post and Company name</th>
            <th scope="col">Image</th>
            <th scope="col">Description</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          @php($i = 1)
          @foreach($teams as $team)
          <tr>
            <th scope="row">{{$i++}}</th>
            <td>{{$team->name}}</td>
            <td>{{$team->post_with_company}}</td>
            <td>Team Image</td>
            <td>{{$team->description}}</td>

            <td>
              <a href = "{{route('team.edit', $team->id)}}" class = "btn btn-success btn-sm">Edit</a>
              <a href = "{{route('team.destroy', $team->id)}}" class = "btn btn-danger btn-sm">Delete</a>

            </td>
          </tr>
          @endforeach
          
        </tbody>
    </table>



</div>
</div>
@stop